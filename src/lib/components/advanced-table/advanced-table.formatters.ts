import {template, at} from 'lodash';
import {TableColumnConfiguration} from './advanced-table.interfaces';
import {Injector} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {DatePipe} from '@angular/common';
import * as ISO6391 from 'iso-639-1/build';
import { ActivatedRoute, NavigationExtras, Router, UrlTree } from '@angular/router';
declare var jQuery: any;
export abstract class AdvancedColumnFormatter implements TableColumnConfiguration {

    public static templateCache = new Map<string, (data: any) => string>();

    protected injector: Injector;
    public className: string;
    public defaultContent: string;
    public formatString: string;
    public formatter: string;
    public hidden: boolean;
    public name: string;
    public order: string;
    public property: string;
    public sortable: boolean;
    public title: string;
    public formatOptions?: any;
    public actions?: Array<any>;

    public static template(expr: string): (data: any) => string {
      let result = AdvancedColumnFormatter.templateCache.get(expr);
      if (typeof result === 'function') {
        return result;
      }
      result = template(expr);
      AdvancedColumnFormatter.templateCache.set(expr, result);
      return result;
    }

    public abstract render(data: any, type: string, row: any, meta: any): any;

    protected createUrlTree(data: any, options: { commands: any[], navigationExtras?: NavigationExtras }): UrlTree {
      if (Array.isArray(options.commands)) {
        const activatedRoute: ActivatedRoute = this.injector.get(ActivatedRoute);
        const router: Router = this.injector.get(Router);
        const commands = options.commands.map( (command) => {
            if (typeof command === 'string') {
                return AdvancedColumnFormatter.template(command)(data);
            }
            if (command.outlets) {
                const commandOutlets = {};
                Object.keys(command.outlets).forEach((key) => {
                    if (Object.prototype.hasOwnProperty.call(command.outlets, key)) {
                        const outlet = command.outlets[key];
                        if (Array.isArray(outlet)) {
                            Object.defineProperty(commandOutlets, key, {
                                enumerable: true,
                                configurable: true,
                                writable: true,
                                value: outlet.map((x) => {
                                    return AdvancedColumnFormatter.template(x)(data);
                                }),
                            });
                        } else {
                            Object.defineProperty(commandOutlets, key, {
                                enumerable: true,
                                configurable: true,
                                writable: true,
                                value: outlet,
                            });
                        }
                    }
                });
                return {
                    outlets: commandOutlets,
                };
            }
            return command;
        });
        const queryParams = {};
        let replaceUrl = true;
        if (options.navigationExtras && options.navigationExtras.queryParams) {
            Object.keys(options.navigationExtras.queryParams).map( (key) => {
              const text = options.navigationExtras.queryParams[key];
              queryParams[key] = AdvancedColumnFormatter.template(text)(data);
            });
        }
        if (options.navigationExtras && typeof options.navigationExtras.replaceUrl !== 'undefined') {
          replaceUrl = options.navigationExtras.replaceUrl;
        }
        let skipLocationChange = false;
        if (options.navigationExtras && typeof options.navigationExtras.skipLocationChange !== 'undefined') {
              skipLocationChange = options.navigationExtras.skipLocationChange;
          }
        return router.createUrlTree(commands, {
            relativeTo: activatedRoute,
            queryParams,
            replaceUrl,
            skipLocationChange,
        });
    }
  }
}

export interface OnAdvancedColumnAfterRender {
  afterRender(cell: any, cellData: any, rowData: any, rowIndex: number, colIndex: number): void;
}

export class NestedPropertyFormatter extends AdvancedColumnFormatter {
    public render(data, type, row, meta) {
        // get column
        const column = meta.settings.aoColumns[meta.col];
        if (column && column.data) {
            return at(row, column.data.replace(/\//g, '.'));
        }
        return data;
    }
    constructor() {
        super();
    }
}

export class ButtonFormatter extends AdvancedColumnFormatter {
  public render(data, type, row, meta) {
    if (data == null) {
      return '';
    }
    // get column
    const column = meta.settings.aoColumns[meta.col];
    // get format options
    const formatOptions = Object.assign({
      buttonClass: 'btn btn-default',
    }, this.formatOptions);
    // get text content
    let buttonText: string;
    let buttonTitle = '';
    if (formatOptions.buttonText) {
      buttonText = this.injector.get(TranslateService).instant(formatOptions.buttonText);
    } else {
      buttonText = formatOptions.buttonContent;
    }
    if (formatOptions.buttonTitle) {
      buttonTitle = this.injector.get(TranslateService).instant(formatOptions.buttonTitle);
    }
    if (Array.isArray(this.formatOptions.commands)) {
      const routerLink = this.createUrlTree(row, this.formatOptions);
      return `<a class="${formatOptions.buttonClass}" title="${buttonTitle}" href="#${routerLink}">${buttonText}</a>`;
    }
    // and return button
    const href = AdvancedColumnFormatter.template(this.formatString)(row);
    return `<a class="${formatOptions.buttonClass}" title="${buttonTitle}" href="${href}">${buttonText}</a>`;
  }
  constructor() {
    super();
  }
}

export class LinkFormatter extends AdvancedColumnFormatter {
  public render(data: any, type: string, row: any, meta: any): any {
    // get column
    const column = meta.settings.aoColumns[meta.col];
    if (column && column.data) {
      return `<a href="${(AdvancedColumnFormatter.template(this.formatString)(row))}">${data}</a>`;
    }
    return data;
  }
  constructor() {
    super();
  }
}

export class ActionLinkFormatter extends AdvancedColumnFormatter
  implements OnAdvancedColumnAfterRender {
  public render(data: any, type: string, row: any, meta: any): any {
    // get column
    const column = meta.settings.aoColumns[meta.col];

    if (column && column.data) {
      const actions = this.actions || [];
      let index = 1;
      for (const action of actions) {
        Object.assign(action, {
          name: action.name || `action${index}`,
        });
        index += 1;
      }
      // return the dropdown menu with 3 dots
      return `<div class='dropdown details-control' data-id='${data}' data-type='student' >
                    <a id="dropdownMenuButton-${data}"
                      data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <span class="fa-2x text-dots"></span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton-${data}">
                     ${actions.map((x) => {
                       if (x.type === 'divider') {
                         return `<div class="dropdown-divider"></div>`;
                       }
                       if (x.formatOptions && x.formatOptions.commands) {
                         const urlTree = this.createUrlTree(row, x.formatOptions);
                         return `<a data-name='${x.name}' class="dropdown-item" href="${urlTree.toString()}">
                            ${this.injector.get(TranslateService).instant(x.title)}
                          </a>`;
                       }
                       return `<a data-name='${x.name}' class="dropdown-item" href="${AdvancedColumnFormatter.template(x.href)(row)}">
                          ${this.injector.get(TranslateService).instant(x.title)}
                        </a>`;
                     })
          .join('')}
                    </div>
                </div>`;
    }
    return data;
  }

  constructor() {
    super();
  }
  public afterRender(cell: any, cellData: any, rowData: any, rowIndex: number, colIndex: number): void {
    // assign bootstrap dropdown (?)
  }
}

export class ActionButtonFormatter extends AdvancedColumnFormatter
  implements OnAdvancedColumnAfterRender {
  public render(data: any, type: string, row: any, meta: any): any {
    // get column
    const column = meta.settings.aoColumns[meta.col];

    if (column && column.data) {
      const actions = this.actions || [];
      let index = 1;
      for (const action of actions) {
        Object.assign(action, {
          name: action.name || `action${index}`,
        });
        index += 1;
      }
      let buttonText: string;
      if (this.formatOptions && this.formatOptions.buttonText) {
        // if button text is a literal expression
        if (this.formatOptions.buttonText.includes('${')) {
          // use template
          buttonText = AdvancedColumnFormatter.template(this.formatOptions.buttonText)(row);
        } else {
          // otherwise translate it
          buttonText = this.injector.get(TranslateService).instant(this.formatOptions.buttonText);
        }
      }
      let buttonTitle: string;
      if (this.formatOptions && this.formatOptions.buttonTitle) {
        buttonTitle = this.injector.get(TranslateService).instant(this.formatOptions.buttonTbuttonTitleext);
      }
      const buttonClass = (this.formatOptions && this.formatOptions.buttonClass) || 'btn btn-light';
      // return the dropdown menu with 3 dots
      return `<div class='dropdown details-control' data-id='${data}'>
                    <a class="${buttonClass} dropdown-toggle" href="#" role="button" title="${buttonTitle}"
                      id="dropdownMenuButton-${data}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      ${buttonText}
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton-${data}">
                     ${actions.map((x) => {
                       if (x.type === 'divider') {
                         return `<div class="dropdown-divider"></div>`;
                       }
                       if (x.commands) {
                         const urlTree = this.createUrlTree(row, x);
                         return `<a class="dropdown-item" href="${urlTree.toString()}">
                            ${this.injector.get(TranslateService).instant(x.title)}
                          </a>`;
                       }
                       return `<a class="dropdown-item" href="${AdvancedColumnFormatter.template(x.href)(row)}">
                          ${this.injector.get(TranslateService).instant(x.title)}
                        </a>`;
                     })
          .join('')}
                    </div>
                </div>`;
    }
    return data;
  }

  constructor() {
    super();
  }
  public afterRender(cell: any, cellData: any, rowData: any, rowIndex: number, colIndex: number): void {
    // assign bootstrap dropdown (?)
  }
}

export class ActionSplitButtonFormatter extends AdvancedColumnFormatter
  implements OnAdvancedColumnAfterRender {
  public render(data: any, type: string, row: any, meta: any): any {
    // get column
    const column = meta.settings.aoColumns[meta.col];

    if (column && column.data) {
      const actions = this.actions || [];
      let index = 1;
      for (const action of actions) {
        Object.assign(action, {
          name: action.name || `action${index}`,
        });
        index += 1;
      }
      if (actions.length === 0) {
        return;
      }
      const buttonClass = (this.formatOptions && this.formatOptions.buttonClass) || 'btn btn-light';
      // return the dropdown menu with 3 dots
      const defaultAction = actions[0];
      let defaultActionHref: string;
      if (defaultAction && defaultAction.commands) {
        defaultActionHref = this.createUrlTree(row, defaultAction).toString();
      } else {
        defaultActionHref = AdvancedColumnFormatter.template(defaultAction.href)(row);
      }
      return `<div class="btn-group">
                    <a type="button" class="${buttonClass}" href="${defaultActionHref}">
                      ${this.injector.get(TranslateService).instant(defaultAction.title)}
                    </a>
                    <button type="button" class="${buttonClass} dropdown-toggle dropdown-toggle-split"
                      data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <span class="sr-only">${this.injector.get(TranslateService).instant('Toggle Dropdown')}</span>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton-${data}">
                     ${actions.map((x) => {
                       if (x.title === '-') {
                         return `<div class="dropdown-divider"></div>`;
                       }
                       if (x.commands) {
                         const urlTree = this.createUrlTree(row, x);
                         return `<a class="dropdown-item" href="${urlTree.toString()}">
                            ${this.injector.get(TranslateService).instant(x.title)}
                          </a>`;
                       }
                       return `<a class="dropdown-item" href="${AdvancedColumnFormatter.template(x.href)(row)}">
                          ${this.injector.get(TranslateService).instant(x.title)}
                        </a>`;
                     })
          .join('')}
                    </div>
                </div>`;
    }
    return data;
  }

  constructor() {
    super();
  }
  public afterRender(cell: any, cellData: any, rowData: any, rowIndex: number, colIndex: number): void {
    // assign bootstrap dropdown (?)
  }
}

export class SelectRowFormatter extends AdvancedColumnFormatter {
    public render(data, type, row, meta) {
        // get column
        const column = meta.settings.aoColumns[meta.col];
        if (column && column.data) {
          const _tpl = `<input class="checkbox checkbox-theme" type="checkbox"
 id="s-${meta.row}" /><label for="s-${meta.row}">&nbsp;</label>`;
          return  AdvancedColumnFormatter.template(_tpl)(row);
        }
        return data;
    }
    constructor() {
        super();
    }
}

export class TemplateFormatter extends AdvancedColumnFormatter {
    public render(data: any, type: string, row: any, meta: any): any {
        // get column
        const column = meta.settings.aoColumns[meta.col];
        if (column && column.data) {
          return AdvancedColumnFormatter.template(this.formatString)(row);
        }
        return data;
    }
    constructor() {
        super();
    }
}

export class TrueFalseFormatter extends AdvancedColumnFormatter {

    public render(data, type, row, meta) {
        return data ? this.injector.get(TranslateService).instant('TrueFalse.long.Yes') :
            this.injector.get(TranslateService).instant('TrueFalse.long.No');
    }

    constructor() {
        super();
    }
}

declare interface TranslationFormatterTemplate {
  translateTemplate?: (data: any) => string;
}

export class TranslationFormatter extends AdvancedColumnFormatter {
  public render(data: any, type: string, row: any, meta: any): any {
    // get column
    const column: TranslationFormatterTemplate = meta.settings.aoColumns[meta.col];
    if (column && data != null) {
      return this.injector.get(TranslateService).instant(
        AdvancedColumnFormatter.template(this.formatString)({
        value: data,
      }));
    }
    return this.injector.get(TranslateService).instant('Unknown');
  }
  constructor() {
    super();
  }
}

export class DateTimeFormatter extends AdvancedColumnFormatter {
  public render(data: any, type: string, row: any, meta: any): any {
    // get column
    const column = meta.settings.aoColumns[meta.col];
    if (column && column.data) {
      const datePipe: DatePipe = new DatePipe(this.injector.get(TranslateService).currentLang);
      if (typeof data === 'string' && /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{2}$/.test(data)) {
        return datePipe.transform(new Date(data + ':00'), this.formatString);
      }
      return datePipe.transform(data, this.formatString);
    }
    return data;
  }
  constructor() {
    super();
  }
}

export class NgClassFormatter extends AdvancedColumnFormatter {
  public render(data, type, row, meta) {
    // get column
    const column = meta.settings.aoColumns[meta.col];
    if (column && column.data) {
      // get formatOptions
      if (this.formatOptions) {
        const ngClass = this.formatOptions.ngClass;
        // check condition for each key
        column.ngClassTemplates = column.ngClassTemplates || {};
        for (const key of Object.keys(ngClass)) {
          const result = AdvancedColumnFormatter.template(ngClass[key])(row);
          const fn = new Function(`return ${result};`);
          // if condition returns true add class
          if (fn() === true) {
            return `<span class="${key}">${data}</span>`;
          }
        }
      }
      return data;
    }
    return data;
  }
  constructor() {
    super();
  }
}
export class LanguageFormatter extends AdvancedColumnFormatter {
  public render(data, type, row, meta) {
    if (data) {
      return ISO6391.getNativeName(data);
    }
    return data;
  }
  constructor() {
    super();
  }
}
