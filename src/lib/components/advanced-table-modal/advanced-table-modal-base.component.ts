import {Component, ElementRef, Input, OnDestroy, OnInit, ViewChild, AfterViewInit} from '@angular/core';
import {RouterModalOkCancel} from '@universis/common/routing';
import {ActivatedRoute, Router} from '@angular/router';
import {TableConfiguration} from '../advanced-table/advanced-table.interfaces';
import {Subscription} from 'rxjs';
import {AdvancedTableComponent, AdvancedTableDataResult} from '../advanced-table/advanced-table.component';
import { AngularDataContext } from '@themost/angular';
import { AdvancedFilterValueProvider } from './../advanced-table/advanced-filter-value-provider.service';
import { DatePipe } from '@angular/common';
import { TemplatePipe } from '@universis/common';
export declare interface PageInfo {
  page: number;
  pages: number;
  start: number;
  end: number;
  length: number;
  recordsTotal: number;
  recordsDisplay: number;
  serverSide?: boolean;
}

// important: export this const due to AOT compilation errors
// see more https://angular.io/guide/aot-metadata-errors#only-initialized-variables
export const AdvancedTableModalBaseTemplate = `
<div>
    <!-- search -->
    <div class="row mt-3">
      <div class="col-12 col-lg-3">
        <div class="input-group mb-3">
          <input spellcheck="false" type="text"
                 (keyup)="onKeyUp($event)" [(ngModel)]="searchText"
                 class="form-control bg-gray-100 border-0"
                 placeholder="{{'Tables.SearchPlaceholder' | translate}}" aria-label="Search" aria-describedby="basic-addon2">
          <div class="input-group-append">
            <button class="btn btn-default bg-gray-100 border-0" type="button">
              <i class="fa fa-search text-secondary"></i>
            </button>
          </div>
        </div>
      </div>
      <div #emptyTable class="d-none">
        <div>
          <div class="mt-4">
            <div class="icon-circle bg-gray-300 border-gray-100"></div>
          </div>
          <div class="mt-4">
            <h4 class="font-3xl text-dark" [translate]="'Tables.EmptyTableTitle'"></h4>
            <p class="font-lg text-dark" [translate]="'Tables.EmptyTableMessage'"></p>
          </div>

        </div>
      </div>
      <div class="col-12 col-lg-5">
        <div class="btn-toolbar">
          <button class="btn btn-gray-100">
            <i class="fa fa-sliders-h pr-2 text-secondary" aria-hidden="true"></i>{{'Tables.Filters' | translate}}
          </button>
          <div #dropdownFilter class="dropdown ml-3">
            <button class="btn btn-gray-100 dropdown-toggle"
                    type="button" id="dropdownDefaultFilters"
                    data-toggle="dropdown" dropdownToggle>
              {{ this.defaultFilter.name | translate }}
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownDefaultFilters">
              <button type="button" class="dropdown-item" *ngFor="let filter of defaultFilters" (click)="onFilterSelect(filter)">
                {{ filter.name | translate }}
              </button>
            </div>
          </div>
        </div>
      </div>
      <div class="col text-lg-right mt-3 mt-lg-0">
        <button [disabled]="true" class="btn rounded-pill btn-outline-theme">
          <span class="pr-2 font-lg">+</span><span [translate]="'Tables.AddItem'"></span>
        </button>
      </div>
    </div>
    <div class="row">
      <div class="col-12" [ngClass]="{ 'component-loading': loading}">
        <app-advanced-table #advancedTable (init)="onTableInit($event)"
                            (load)="onDataLoad($event)"
                            [selectable]="true"
                            [multipleSelect]="true"
                            [scrollX]="false" [scrollY]="320"
                            [pageLength]="pageLength"
                            [scrollable]="false" [showFooter]="false"
                            [showActions]="false" [lengthMenu]="lengthMenu"
                            [config]="tableConfig" [configSrc]="tableConfigSrc"></app-advanced-table>
      </div>
    </div>
    <div class="row mt-3 table-custom-pager">
      <div class="col-12 d-flex" *ngIf="recordsTotal">
        <div class="mt-2 ml-auto mr-3 text-secondary" [translate]="'Tables.RecordsPerPage'"></div>
        <div #dropdownLength class="dropdown mr-3">
          <button class="btn btn-gray-100 dropdown-toggle"
                  type="button" id="dropdownPageSize" data-toggle="dropdown"
                  dropdownToggle>
            {{pageLength}}
          </button>
          <div class="dropdown-menu" aria-labelledby="dropdownPageSize">
            <button (click)="setLength(item)"
                    class="dropdown-item dropdown-item-theme"
                    type="button" *ngFor="let item of lengthMenu">{{item}}</button>
          </div>
        </div>
        <div class="mt-2 mr-2" [translate]="'Tables.PageInfo'" [translateParams]="pageInfo"></div>
        <button [disabled]="pageInfo && pageInfo.page === 0" class="btn text-nowrap" (click)="firstPage()">
          <i class="fa fa-chevron-left text-color-theme"></i>
          <i class="fa fa-chevron-left text-color-theme"></i>
        </button>
        <button [disabled]="pageInfo && pageInfo.page===0" class="btn" (click)="previousPage()">
          <i class="fa fa-chevron-left text-color-theme"></i>
        </button>
        <button [disabled]="pageInfo && pageInfo.page===pageInfo.pages-1" class="btn" (click)="nextPage()">
          <i class="fa fa-chevron-right text-color-theme"></i>
        </button>
        <button [disabled]="pageInfo && pageInfo.page===pageInfo.pages-1" class="btn text-nowrap" (click)="lastPage()">
          <i class="fa fa-chevron-right text-color-theme"></i>
          <i class="fa fa-chevron-right text-color-theme"></i>
        </button>
      </div>
    </div>
  </div>
`;

/**
 *
 * A specific value for a criterion
 *
 */
interface SearchItem {
  filter: any;
  name: string;
}

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'app-advanced-table-modal',
  template: AdvancedTableModalBaseTemplate,
  styles: [
    `

    `
  ]
})
export class AdvancedTableModalBaseComponent extends RouterModalOkCancel implements OnInit, OnDestroy, AfterViewInit {
  ngAfterViewInit(): void {
    //
  }

  private static readonly DEFAULT_ALL_FILTER = {
    name: 'Tables.All',
    filter: null
  };

  @ViewChild('advancedTable') advancedTable: AdvancedTableComponent;
  @ViewChild('emptyTable') emptyTable: ElementRef;
  @ViewChild('dropdownFilter') dropdownFilter: ElementRef;
  @ViewChild('dropdownLength') dropdownLength: ElementRef;
  @Input('tableConfig') tableConfig: TableConfiguration;
  @Input('tableConfigSrc') tableConfigSrc: string;
  @Input('pageLength') pageLength = 25;
  @Input('searchText') searchText: string;
  @Input('lengthMenu') lengthMenu: Array<number> = [5, 10, 25, 50, 100];
  private subscription: Subscription;
  public recordsTotal = 0;
  public loading = false;
  public recordsFiltered = 0;
  public defaultFilters: Array<any> = [AdvancedTableModalBaseComponent.DEFAULT_ALL_FILTER];
  @Input('defaultFilter') defaultFilter = AdvancedTableModalBaseComponent.DEFAULT_ALL_FILTER;
  public pageInfo = <PageInfo> {
    serverSide: true
  };
  private _template: TemplatePipe;
  configChangesSubscription: Subscription;

  constructor(
    _router: Router,
    _activatedRoute: ActivatedRoute,
    protected _context: AngularDataContext,
    protected advancedFilterValueProvider: AdvancedFilterValueProvider,
    protected datePipe: DatePipe
  ) {
    super(_router, _activatedRoute);
    this.modalClass = 'modal-xl modal-table';
    this._template = new TemplatePipe();
  }

  private _getInputs(componentType): Array<any> {
    const props = componentType.__prop__metadata__;
    const inputs = [];
    if (props == null) {
      return inputs;
    }
    Object.keys(props).forEach (prop => {
      const member = props[prop][0];
      if (member.ngMetadataName === 'Input') {
        inputs.push(prop);
      }
    });
    return inputs.sort();
  }

  public hasInputs(): Array<string> {
    return [];
  }

  ngOnInit() {

    this.okButtonText = 'Tables.Apply';
    this.cancelButtonText = 'Tables.Cancel';

    // get route data
    this.subscription = this.activatedRoute.data.subscribe( data => {
      // set modal title if any
      if (data.title) {
        this.modalTitle = data.title;
      }

      if (data.config) {
        this.tableConfig = data.config;
      }
      if (this.tableConfig) {
        this.defaultFilters.push.apply(this.defaultFilters, this.tableConfig.searches);
      }
      // important: use hasInputs due to AOT compilation. Metadata properties cannot be used (e.g. @Input())
      // because there are empty during runtime
      // so we need to define manually input properties that are going to be bind through this process
      // bind all inputs
      const inputs = this.hasInputs();
      inputs.forEach( input => {
        if (Object.prototype.hasOwnProperty.call(data, input)) {
          this[input] = data[input];
        }
      });

      this.configChangesSubscription = this.advancedTable.configChanges.subscribe((config) => {
        if (this.tableConfig) {
          // reset default filter
          this.defaultFilter = AdvancedTableModalBaseComponent.DEFAULT_ALL_FILTER;
          // init filter
          this.defaultFilters = [AdvancedTableModalBaseComponent.DEFAULT_ALL_FILTER];
          // add extra filters
          this.defaultFilters.push.apply(this.defaultFilters, this.tableConfig.searches);
        }
      });

    });
  }

  cancel(): Promise<any> {
    return this.close();
  }

  ok(): Promise<any> {
    return this.close();
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
    this.recordsFiltered = data.recordsFiltered;
    // get first page info (because page is not going to be fired for first page)
    if (data.recordsTotal > 0 && this.pageInfo.recordsTotal === 0) {
      this.pageInfo.recordsTotal = data.recordsTotal;
    }
    const self = this;
  }

  previousPage() {
    // go to next page
    // noinspection TypeScriptValidateJSTypes
    this.advancedTable.dataTable.page('previous').draw('page');
  }

  nextPage() {
    // go to next page
    // noinspection TypeScriptValidateJSTypes
    this.advancedTable.dataTable.page('next').draw('page');
  }

  onTableInit(dataTable: any) {
    const self = this;
    // get initial page info
    const initialPageInfo = <PageInfo>dataTable.page.info();
    initialPageInfo.start += 1;
    self.pageInfo = initialPageInfo;
    // change emptyTableMessage
    dataTable.context[0].oLanguage.sEmptyTable = this.emptyTable.nativeElement.innerHTML;
    // bind page change event
    dataTable.on( 'page.dt', function() {
      const pageInfo = <PageInfo>dataTable.page.info();
      pageInfo.start += 1;
      self.pageInfo = pageInfo;
    });
    // bind draw change event
    dataTable.on( 'draw.dt', function() {
      // for future use
      self.loading = false;
      const pageInfo = <PageInfo>dataTable.page.info();
      pageInfo.start += 1;
      self.pageInfo = pageInfo;
    });
  }

  firstPage() {
    // go to first page
    // noinspection TypeScriptValidateJSTypes
    this.advancedTable.dataTable.page('first').draw('page');
  }

  lastPage() {
    // go to last page
    // noinspection TypeScriptValidateJSTypes
    this.advancedTable.dataTable.page('last').draw('page');
  }

    setLength(value: number) {
      this.pageLength = value;
      this.advancedTable.dataTable.page.len(value).draw();
    }

  onKeyUp(event: KeyboardEvent) {
    if (event.code === 'Enter') {
      // loading
      this.loading = true;
      this.advancedTable.search((<HTMLInputElement>event.target).value);
    }
  }

  /**
   *
   * Reads the modal table and returns a list of filters to use in the table
   * query.
   *
   * @param tableConfig {object} The table configuration from json file
   * @param defaultFilterValues {object} A list of default filters
   * @param {object} searchItem The searchItem to use
   *
   */
  getFilters(
    tableConfig: any,
    defaultFilterValues = {},
    searchItem: any
  ): Array<string> {
    // Do nothing in absence of configuration.
    if (!tableConfig || !Array.isArray(tableConfig.criteria)) {
      return [];
    }

    if (searchItem.filter == null) {
      return [];
    }

    const matchedCriterion = tableConfig.criteria.find(criterion =>
      Object.prototype.hasOwnProperty.call(searchItem.filter, criterion.name)
    );

    if (matchedCriterion == null) {
      return [];
    }

    const value = searchItem.filter[matchedCriterion.name];

    const criterionSafeName = matchedCriterion.name !== null
      ? matchedCriterion.name.toString()
      : '';

    let applicableFilter: string;

    // Raw dates should be transformed to a specific format
    if (criterionSafeName.includes('GMT')) {
      const newDate = this.datePipe.transform(criterionSafeName, 'MM/dd/yyyy');
      applicableFilter = this._template.transform(matchedCriterion.filter, {
        value: newDate
      });

    } else if (criterionSafeName !== 'undefined') {
      applicableFilter = (this._template.transform(matchedCriterion.filter, Object.assign({
        value: value
      }, defaultFilterValues)));
    } else {
      applicableFilter = '';
    }

    return [applicableFilter];
  }

  onFilterSelect(value: { filter: any; name: string } | any) {
    this.advancedSearch(value);
  }

  /**
   *
   * Runs a filter against the modal table
   *
   * @param {object} filter The filter to use in the search
   *
   */
  async advancedSearch(filter): Promise<any> {
    // Do nothing in absence of configuration.
    if (!this.tableConfig && !Array.isArray(this.tableConfig.criteria)) {
      console.warn('No table configuration available', this.tableConfig);
      return;
    }

    // An advanced table has some filters by default
    let defaultFilterValues;
    try {
      defaultFilterValues = await this.advancedFilterValueProvider.getValues();
    } catch (err) {
      defaultFilterValues = {};
    }

    const expressions =  [];
    let predefinedCriteria;
    if (filter) {
      this.defaultFilter = filter;
      predefinedCriteria = this.getFilters(
        this.tableConfig,
        defaultFilterValues,
        filter
      );
    } else {
      // Empty a falsy filter resets the results
      this.defaultFilter = AdvancedTableModalBaseComponent.DEFAULT_ALL_FILTER;
    }

    // search filter is applied separately, there is no need to add here.
    expressions.push(predefinedCriteria);

    const query = this._context.model(this.advancedTable.config.model).asQueryable();
    query.setParam('filter', expressions.join(' and ')).prepare();
    this.advancedTable.query = query;
    this.advancedTable.fetch();
  }
}

