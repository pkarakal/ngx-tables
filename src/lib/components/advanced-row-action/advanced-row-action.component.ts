import {Component, OnInit, OnDestroy, Input, EventEmitter, ViewChild, ElementRef, ViewEncapsulation, OnChanges, SimpleChanges, AfterViewInit, Output} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import 'rxjs/add/observable/combineLatest';
import {ErrorService, ModalService, ToastService} from '@universis/common';
import {RouterModalOkCancel} from '@universis/common/routing';
import {AppEventService} from '@universis/common';
import {Observable, Subscription} from 'rxjs';
import {AdvancedFormComponent} from '@universis/forms';
import { ProgressbarComponent } from 'ngx-bootstrap/progressbar';

@Component({
    selector: 'app-advanced-row-action',
    templateUrl: './advanced-row-action.component.html',
    styles: [
        `
        .fa-1_5x {
            font-size: 1.5rem;
        }
        .form-control:disabled, .form-control[readonly] {
            background-color: inherit;
        }
        .form-check.checkbox {
          position: relative
        }
        `
    ],
    encapsulation: ViewEncapsulation.None
})
export class AdvancedRowActionComponent extends RouterModalOkCancel implements OnInit, AfterViewInit, OnDestroy {

  invalidSubscription: any;


    @Input() items: any;
    @Input() modalIcon = 'fas fa-project-diagram';
    @Input() description: string;
    @Input() formTemplate: string;
    @Input() progressType: string = 'indigo';
    @Output() data: any;
    @Input() additionalMessage: string;
    @Input() errorMessage = 'Tables.Actions.CompletedWithErrors';
    public lastError: any;
    public loading = false;
    @ViewChild('progress') progressComponent: ProgressbarComponent;
    @ViewChild('formComponent') formComponent: AdvancedFormComponent;
    @Input() refresh: any = new EventEmitter<any>();
    @Input() execute: Observable<any>;
    private refreshSubscription: Subscription;
    private executeSubscription: Subscription;

    constructor(router: Router,
                activatedRoute: ActivatedRoute,
                private _translateService: TranslateService,
                private _context: AngularDataContext,
                private _errorService: ErrorService,
                private _modalService: ModalService,
                private _appEvent: AppEventService) {
        super(router, activatedRoute);
        // set modal size
        this.modalClass = 'modal-lg';
        // set modal title
        this.modalTitle = 'Tables.Actions.Label';
        // disable ok button
        this.okButtonDisabled = false;
        // set button text
        this.okButtonText = 'Tables.Actions.Start';
    }

    get form() {
      return this.formComponent;
    }
    ngOnInit() {
        this.refreshSubscription = this.refresh.subscribe((value) => {
            if (value && value.progress) {
                let progress = 0;
                if (value.progress < 0) {
                    progress = 0;
                } else if (value.progress > 100) {
                    progress = 100;
                } else {
                    progress = value.progress;
                }
                this.progressComponent.value = progress;
            }
        });

    }

    ngAfterViewInit(): void {
      if (this.formComponent && this.formComponent.form) {
        this.okButtonDisabled = true;
        this.invalidSubscription = this.formComponent.form.change.subscribe((event: any) => {
           if (Object.prototype.hasOwnProperty.call(event, "isValid") && (event.changed != null)) {
              this.okButtonDisabled = !event.isValid;
           }
        });
      }
    }

    ngOnDestroy() {
        if (this.refreshSubscription) {
            this.refreshSubscription.unsubscribe();
        }
        if (this.executeSubscription) {
            this.executeSubscription.unsubscribe();
        }
        if (this.invalidSubscription) {
            this.invalidSubscription.unsubscribe();
        }
    }

    cancel(): Promise<any> {
        if (this.loading) {
            return;
        }
        // close
        if (this._modalService.modalRef) {
            return  this._modalService.modalRef.hide();
        }
    }

    async ok() {
        try {
            this.loading = true;
            this.lastError = null;
            if (this.formComponent && this.formComponent.form &&
              this.formComponent.form.formio && this.formComponent.form.formio.data) {
              // set data
                  this.data = this.formComponent.form.formio.data;
                }
            // do action
            this.execute.subscribe((result) => {
                this.loading = false;
                this.refresh.emit({
                    progress: 100
                });
                if (result && result.errors && result.errors > 0) {
                    // show message for partial success
                    if (result.errors === 1) {
                        this.lastError = new Error(this._translateService.instant(
                            `${this.errorMessage}.Description.One`));
                    } else {
                        this.lastError = new Error(this._translateService.instant(
                          `${this.errorMessage}.Description.Many`,
                            result));
                    }
                    this.refresh.emit({
                        progress: 1
                    });
                    return;
                }
                if (this._modalService.modalRef) {
                    this._modalService.modalRef.hide();
                }
            }, (err) => {
                this.loading = false;
                this.lastError = err;
                this.refresh.emit({
                    progress: 0
                });
            });
        } catch (err) {
            this.loading = false;
            this.lastError = err;
        }
    }


}
