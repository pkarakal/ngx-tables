import { Injectable } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { TemplatePipe } from '@universis/common';

@Injectable({
  providedIn: 'root'
})
export class AdvancedAggregatorService {

  constructor(
    private _context: AngularDataContext,
    private _template: TemplatePipe
  ) { }

  /**
   *
   * Formats and executes the data query
   *
   * @param {string} model The target model
   * @param {object} fixedParams Pairs of parameters to always be applied
   * @param {string} queryStatement The default query to use for fetching the data (e.g. expands etc)
   * @param {Array<string>} filters A list of filter expressions to append at the query
   * @param {string} searchTemplate The template to use for searching expressions
   * @param {string} searchValue The value of the user search
   *
   * @returns {Promise<any>} The response of the query execution
   */
  async fetchData(model: string, fixedParams: object, queryStatement: string = '', filters: string[], searchTemplate?: string, searchValue?: string): Promise<any> {
    const query = this._context.model(model).asQueryable();

    if (!fixedParams || typeof fixedParams !== 'object') {
      fixedParams = {};
    }

    let filterExpressions = [];

    if (fixedParams) {
      for (let param of Object.keys(fixedParams)) {
        filterExpressions.push(`${param} eq ${fixedParams[param]}`);
      }
    }

    if (filters && Array.isArray(filters)) {
      filterExpressions = [
        ...filterExpressions,
        ...filters
      ]
    }

    if (searchTemplate && searchValue) {
      let evaluatedSearch;
      if (/^"(.*?)"$/.test(searchValue)) { // validate search text in double quotes
        evaluatedSearch = this._template.transform(searchTemplate, {
          ...fixedParams,
          text: searchValue.replace(/^"|"$/g, '')
        });
      } else {
        // try to split words
        const words = searchValue.split(' ');
        // join words to a single filter
        evaluatedSearch = words.map(word => {
          return this._template.transform(searchTemplate, {
            ...fixedParams,
            text: word
          });
        }).join(' and ');
      }
      filterExpressions.push(evaluatedSearch);
    }
    query.setParam('$filter', filterExpressions.join(' and '));
    return query.expand(queryStatement).take(-1).getItems();
  }

  /**
   *
   * Formats the received data to use flat keys
   *
   * @param config The applied configuration
   * @param data The raw data
   *
   * @returns {Array<any>} The flat-key object list
   *
   */
  formatData(config, data) {
    try {
      return data.map((entry) => {
        let entryObject = {} as any;
        for (let i = 0; i < config.columns.length; i++) {
          const keys = config.columns[i].name.split('/');
          let helperObject = entry[keys[0]];

          if (helperObject && keys.length > 1) {
            for (let i = 1; i < keys.length; i++) {
              if (helperObject === undefined || helperObject === null) {
                break;
              } else {
                helperObject = helperObject[keys[i]] !== undefined && helperObject[keys[i]] !== null
                  ? helperObject[keys[i]]
                  : undefined;
              }
            }
          }

          entryObject[config.columns[i].name] = helperObject;
        }

        if (entry.subitems) {
          entryObject.subitems = entry.subitems;
        }

        return entryObject;
      });
    } catch (err) {
      console.error(err);
    }
  }
}
